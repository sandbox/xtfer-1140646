<?php
/**
 * Abstract regionLayout
 * Other layouts should extend regionLayout
 */
class Layout {
  // Title for use in Admin interface
  public $defaults;
  public $theme;
  public $regions;
  public $render;
  public $assets;
  public $attributes;
  
  // Original config data
  protected $config;
  
  /**
   * Construct
   */
  public function __construct($config){
    $this->defaults->title = $config['#title'];
    $this->defaults->name = $config['#name'];
    $this->defaults->category = $config['#category'];
    $this->defaults->type = $config['#type'];
    $this->defaults->provider = $config['#provider'];
    
    $this->theme = $config['#theme'];
    
    $this->assets = $config['#assets'];
    
    $this->attributes->raw = array();
    
    foreach($config['#regions'] as $key => $readable){
      $this->regions[$key] = NULL;
    }
  }
  
  /**
   * Assign content to a region
   */
  public function content($region, $content){
    $this->regions[$region] = $content;
  }
  
  /**
   * Assemble any content in regions into a rendered layout
   */
  public function build(){
    $this->add_assets();
    $this->process_attributes();
    
    $theme_function = key($this->theme);
    
    foreach($this->stylesheets as $sheet){
      drupal_add_css($sheet);
    }
    $this->render = theme($theme_function, $this);
  }
  
  /**
   * Attributes
   */
   protected function process_attributes(){
     $this->attributes->id = '';
     $this->attributes->class = '';
   }
   
  /**
   * CSS
   */
  public function add_css($stylesheet){
    $this->stylesheets[] = $stylesheet;
  }
  
  /**
   * Assets
   */
  protected function add_assets(){
    foreach($this->assets as $type => $assets){
      if($type == '#css'){
        foreach($assets as $asset){
          $this->stylesheets[] = $this->defaults->provider['path'] .'/'. $asset;
        }
      }
      elseif($type == '#js'){
        foreach($assets as $asset){
          $this->js[] = $this->defaults->provider['path'] .'/'. $asset;
        }
      }
    }
  }
  
  /**
   * Render content
   */
  public function render(){
    return $this->render;
  }
}
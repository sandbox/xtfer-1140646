<?php
/**
 * Example of a Layout API template file
 *
 * $layout->pane['name'] inserts a given pane
 */
?>

<div class="layout-wrapper">
  <div class="layout-row">
    <div class="layout-column">
      <?php echo $layout->regions->region['content']; ?>
    </div>
  </div>
</div>
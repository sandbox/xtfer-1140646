
 ===============
 = Layouts API =
 ===============
 
This code is still in development. The API below will not change, however its
usage might.

Declare available layouts:
HOOK_define_layouts()

Return a specific layout:
$my_layout = layouts_load($layout_name)

Assign content to a layout region:
$my_layout->content($region, $content)

Assemble any region content into a formatted layout:
$my_layout->build()

Render a layout:
$my_layout->render()

